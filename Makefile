CI_REGISTRY_IMAGE ?= registry.gitlab.com/ulrichschreiner/nexus-cacs-plugin
NEXUS_VERSION ?= 3.33.1
PLUGIN_VERSION ?= 3.33.0.1

.phony:
default: build-nexus ;

.phony:
build-nexus:
	echo $(PLUGIN_VERSION)
	docker build -t $(CI_REGISTRY_IMAGE):$(NEXUS_VERSION) \
		--build-arg NEXUS_VERSION=$(NEXUS_VERSION) \
		--build-arg PLUGIN_VERSION=$(PLUGIN_VERSION) \
		.
