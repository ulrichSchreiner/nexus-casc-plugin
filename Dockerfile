ARG NEXUS_VERSION
FROM sonatype/nexus3:${NEXUS_VERSION}
ARG PLUGIN_VERSION
USER root
RUN set -eux; \
    curl -L -f -o /opt/sonatype/nexus/deploy/nexus-casc-plugin-${PLUGIN_VERSION}-bundle.kar \
        https://repo1.maven.org/maven2/io/github/asharapov/nexus/nexus-casc-plugin/${PLUGIN_VERSION}/nexus-casc-plugin-${PLUGIN_VERSION}-bundle.kar;
USER nexus